const BASE_URL = 'http://localhost:8080/api';

export const getCollegeState = async () => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college/state`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};

export const getCollegeCourse = async () => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college/course`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};

export const getColleges = async (params = '') => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college${params}`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};

export const getCollegeDetail = async (_id) => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college/${_id}`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};

export const getSimilarColleges = async (_id) => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college/s/${_id}`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};

export const getStudentDetail = async (_id) => {
  const requestOptions = {
    method: 'GET'
  };
  return fetch(`${BASE_URL}/college/student/${_id}`, requestOptions)
    .then((response) => response.json())
    .then((result) => result)
    .catch((error) => console.log('error', error));
};
