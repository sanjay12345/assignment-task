import { Navigate, useRoutes } from 'react-router-dom';
// layouts
import DashboardLayout from './layouts/dashboard';
//
import DashboardApp from './pages/DashboardApp';
import College from './pages/College';
import NotFound from './pages/Page404';
import CollegeDetail from './pages/CollegeDetail';
import StudentDetail from './pages/StudentDetail';

// ----------------------------------------------------------------------

export default function Router() {
  return useRoutes([
    {
      path: '/',
      element: <DashboardLayout />,
      children: [
        { element: <Navigate to="/app" replace /> },
        { path: 'app', element: <DashboardApp /> },
        { path: 'college', element: <College /> },
        { path: 'college/:_id', element: <CollegeDetail /> },
        { path: 'student/:_id', element: <StudentDetail /> },
        { path: '404', element: <NotFound /> }
      ]
    },
    { path: '*', element: <Navigate to="/404" replace /> }
  ]);
}
