import { useState } from 'react';
import { Outlet } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import { Box, Button } from '@mui/material';

import { Icon } from '@iconify/react';
import menuOutline from '@iconify/icons-eva/menu-outline';
//
import { MHidden } from '../../components/@material-extend';
import DashboardSidebar from './DashboardSidebar';
// ----------------------------------------------------------------------

const APP_BAR_MOBILE = 0;
const APP_BAR_DESKTOP = 0;

const RootStyle = styled('div')({
  display: 'flex',
  minHeight: '100%',
  overflow: 'hidden'
});

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP + 24,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  }
}));

// ----------------------------------------------------------------------

export default function DashboardLayout() {
  const [open, setOpen] = useState(false);

  return (
    <RootStyle>
      <DashboardSidebar isOpenSidebar={open} onCloseSidebar={() => setOpen(false)} />
      <MainStyle>
        <MHidden width="lgUp">
          <Box sx={{ pt: 2, pb: 2, textAlign: 'left' }}>
            <Button
              size="small"
              color="inherit"
              onClick={() => setOpen(!open)}
              endIcon={<Icon icon={menuOutline} />}
            />
          </Box>
        </MHidden>
        <Outlet />
      </MainStyle>
    </RootStyle>
  );
}
