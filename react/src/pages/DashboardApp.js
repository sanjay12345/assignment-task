// material
import { Grid, Container } from '@mui/material';
import { useEffect, useState } from 'react';
// components
import Page from '../components/Page';
import { AppChart } from '../components/_dashboard/app';
import { getCollegeCourse, getCollegeState } from '../services/api';

// ----------------------------------------------------------------------

export default function DashboardApp() {
  const [states, setStates] = useState([]);
  const [statesData, setStatesData] = useState([]);
  const [statesColors, setStatesColors] = useState([]);
  const [courses, setCourses] = useState([]);
  const [coursesData, setCoursesData] = useState([]);
  const [coursesColors, setCoursesColors] = useState([]);

  useEffect(() => {
    if (states.length > 0) return;
    fetchState();
    // eslint-disable-next-line
  }, [states]);

  useEffect(() => {
    if (courses.length > 0) return;
    fetchCourse();
    // eslint-disable-next-line
  }, [courses]);

  const fetchState = async () => {
    const collegeState = await getCollegeState();
    const s = [];
    const sd = [];
    const c = [];
    for (let i = 0; i < collegeState.data.length; i++) {
      const element = collegeState.data[i];
      c.push(genrateColor(c));
      s.push(element._id);
      sd.push(element.count);
    }
    setStates(s);
    setStatesData(sd);
    setStatesColors(c);
  };

  const fetchCourse = async () => {
    const collegeCourse = await getCollegeCourse();
    const s = [];
    const sd = [];
    const c = [];
    for (let i = 0; i < collegeCourse.data.length; i++) {
      const element = collegeCourse.data[i];
      c.push(genrateColor(c));
      s.push(element._id);
      sd.push(element.count);
    }
    setCourses(s);
    setCoursesData(sd);
    setCoursesColors(c);
  };

  const genrateColor = (c) => {
    const color = getRandomColor();
    if (c.indexOf(color) !== -1) {
      genrateColor(c);
    }
    return color;
  };

  const getRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };

  return (
    <Page title="Dashboard">
      <Container maxWidth="xl">
        <Grid container rowSpacing={3}>
          {states.length > 0 && statesData.length > 0 && (
            <Grid item xs={12} md={12} lg={12}>
              <AppChart
                title="Charts by State"
                type="state"
                CHART_DATA={statesData}
                labels={states}
                colors={statesColors}
              />
            </Grid>
          )}
          {courses.length > 0 && coursesData.length > 0 && (
            <Grid item xs={12} md={12} lg={12}>
              <AppChart
                title="Charts by Courses"
                type="course"
                CHART_DATA={coursesData}
                labels={courses}
                colors={coursesColors}
              />
            </Grid>
          )}
        </Grid>
      </Container>
    </Page>
  );
}
