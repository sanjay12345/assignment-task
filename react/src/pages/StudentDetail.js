import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import { Grid, Card, Toolbar, Stack, Container, Typography } from '@mui/material';
// components
import Page from '../components/Page';
//
import { getStudentDetail } from '../services/api';

const RootStyle = styled(Toolbar)(({ theme }) => ({
  height: 96,
  display: 'flex',
  justifyContent: 'space-between',
  padding: theme.spacing(0, 1, 0, 3)
}));

const TitleStyle = styled('span')(({ theme }) => ({
  fontWeight: 'bold',
  padding: theme.spacing(0, 1, 0, 3)
}));

const TextStyle = styled('span')(({ theme }) => ({
  padding: theme.spacing(0, 1, 0, 0)
}));

export default function StudentDetail() {
  const [student, setStudent] = useState({});
  const params = useParams();

  useEffect(() => {
    fetchStudent(params._id);
  }, [params._id]);

  const fetchStudent = async (_id) => {
    const studentDetail = await getStudentDetail(_id);
    console.log(studentDetail.data);
    setStudent(studentDetail.data);
  };

  const { name, year_of_batch: yearOfBatch, skills } = student;

  return (
    <Page title="Student">
      <Container maxWidth="xl">
        <Grid container rowSpacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <Card>
              <RootStyle>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                  <Typography variant="h4" gutterBottom>
                    Student Details
                  </Typography>
                </Stack>
              </RootStyle>
              <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} mb={5}>
                <Grid item xs={12} md={6}>
                  <TitleStyle>Name:</TitleStyle>
                  <TextStyle>{name}</TextStyle>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TitleStyle>Year of batch:</TitleStyle>
                  <TextStyle>{yearOfBatch}</TextStyle>
                </Grid>
                <Grid item xs={12} md={12}>
                  <TitleStyle>Skills:</TitleStyle>
                  <TextStyle>{skills && skills.map((c) => c.name).join(', ')}</TextStyle>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
