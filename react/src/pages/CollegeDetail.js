import { filter } from 'lodash';
import { useEffect, useState } from 'react';
import { Link as RouterLink, useParams } from 'react-router-dom';
// material
import { styled } from '@mui/material/styles';
import {
  Grid,
  Card,
  Table,
  Toolbar,
  Stack,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
// components
import Page from '../components/Page';
import Scrollbar from '../components/Scrollbar';
import SearchNotFound from '../components/SearchNotFound';
import { UserListHead, UserListToolbar } from '../components/_dashboard/user';
//
import { getCollegeDetail, getSimilarColleges } from '../services/api';

// ----------------------------------------------------------------------

const TABLE_HEAD_S = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'year_of_batch', label: 'Year Of Batch', alignRight: false }
];

const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'year_founded', label: 'Year Founded', alignRight: false },
  { id: 'city', label: 'City', alignRight: false },
  { id: 'state', label: 'State', alignRight: false }
];

// ----------------------------------------------------------------------

const RootStyle = styled(Toolbar)(({ theme }) => ({
  height: 96,
  display: 'flex',
  justifyContent: 'space-between',
  padding: theme.spacing(0, 1, 0, 3)
}));

const TitleStyle = styled('span')(({ theme }) => ({
  fontWeight: 'bold',
  padding: theme.spacing(0, 1, 0, 3)
}));

const TextStyle = styled('span')(({ theme }) => ({
  padding: theme.spacing(0, 1, 0, 0)
}));

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(array, (_user) => _user.name.toLowerCase().indexOf(query.toLowerCase()) !== -1);
  }
  return stabilizedThis.map((el) => el[0]);
}

export default function CollegeDetail() {
  const [spage, setSPage] = useState(0);
  const [sorder, setSOrder] = useState('asc');
  const [sorderBy, setSOrderBy] = useState('name');
  const [sfilterName, setSFilterName] = useState('');
  const [srowsPerPage, setSRowsPerPage] = useState(5);

  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [orderBy, setOrderBy] = useState('name');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);

  const [colleage, setColleage] = useState({});
  const [studentList, setStudentList] = useState([]);
  const [collegeList, setCollegeList] = useState([]);

  const params = useParams();

  useEffect(() => {
    fetchCollege(params._id);
    fetchSimilarColleges(params._id);
  }, [params._id]);

  const fetchCollege = async (_id) => {
    const collegeDetail = await getCollegeDetail(_id);
    console.log(collegeDetail.data);
    setColleage(collegeDetail.data.college);
    setStudentList(collegeDetail.data.students);
  };

  const fetchSimilarColleges = async (_id) => {
    const colleges = await getSimilarColleges(_id);
    setCollegeList(colleges.data);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const handleSRequestSort = (event, property) => {
    const isAsc = sorderBy === property && sorder === 'asc';
    setSOrder(isAsc ? 'desc' : 'asc');
    setSOrderBy(property);
  };

  const handleSChangePage = (event, newPage) => {
    setSPage(newPage);
  };

  const handleSChangeRowsPerPage = (event) => {
    setSRowsPerPage(parseInt(event.target.value, 10));
    setSPage(0);
  };

  const handleSFilterByName = (event) => {
    setSFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - collegeList.length) : 0;

  const filteredColleges = applySortFilter(collegeList, getComparator(order, orderBy), filterName);

  const isCollegeNotFound = filteredColleges.length === 0;

  const semptyRows = page > 0 ? Math.max(0, (1 + spage) * srowsPerPage - studentList.length) : 0;

  const filteredStudents = applySortFilter(
    studentList,
    getComparator(sorder, sorderBy),
    sfilterName
  );

  const isStudentNotFound = filteredStudents.length === 0;

  const { name, year_founded: yearFounded, city, state, courses } = colleage;

  return (
    <Page title="College">
      <Container maxWidth="xl">
        <Grid container rowSpacing={3}>
          <Grid item xs={12} md={12} lg={12}>
            <Card>
              <RootStyle>
                <Stack direction="row" alignItems="center" justifyContent="space-between">
                  <Typography variant="h4" gutterBottom>
                    College Details
                  </Typography>
                </Stack>
              </RootStyle>
              <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }} mb={5}>
                <Grid item xs={12} md={6}>
                  <TitleStyle>Name:</TitleStyle>
                  <TextStyle>{name}</TextStyle>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TitleStyle>Year founded:</TitleStyle>
                  <TextStyle>{yearFounded}</TextStyle>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TitleStyle>City:</TitleStyle>
                  <TextStyle>{city}</TextStyle>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TitleStyle>State:</TitleStyle>
                  <TextStyle>{state}</TextStyle>
                </Grid>
                <Grid item xs={12} md={12}>
                  <TitleStyle>Courses:</TitleStyle>
                  <TextStyle>{courses && courses.map((c) => c.name).join(', ')}</TextStyle>
                </Grid>
              </Grid>
            </Card>
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Card>
              <UserListToolbar
                title="Students"
                filterName={sfilterName}
                onFilterName={handleSFilterByName}
              />

              <Scrollbar>
                <TableContainer sx={{ minWidth: 800 }}>
                  <Table>
                    <UserListHead
                      order={sorder}
                      orderBy={sorderBy}
                      headLabel={TABLE_HEAD_S}
                      rowCount={studentList.length}
                      onRequestSort={handleSRequestSort}
                    />
                    <TableBody>
                      {filteredStudents
                        .slice(spage * srowsPerPage, spage * srowsPerPage + srowsPerPage)
                        .map((row) => {
                          const { _id, name, year_of_batch: yearOfBatch } = row;

                          return (
                            <TableRow hover key={_id} tabIndex={-1}>
                              <TableCell component="th" scope="row">
                                <Stack direction="row" alignItems="center" spacing={2}>
                                  <Typography variant="subtitle2" noWrap>
                                    <RouterLink to={`/student/${_id}`}>{name}</RouterLink>
                                  </Typography>
                                </Stack>
                              </TableCell>
                              <TableCell align="left">{yearOfBatch}</TableCell>
                            </TableRow>
                          );
                        })}
                      {semptyRows > 0 && (
                        <TableRow style={{ height: 53 * semptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    {isStudentNotFound && (
                      <TableBody>
                        <TableRow>
                          <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                            <SearchNotFound searchQuery={filterName} />
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    )}
                  </Table>
                </TableContainer>
              </Scrollbar>

              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={studentList.length}
                rowsPerPage={srowsPerPage}
                page={spage}
                onPageChange={handleSChangePage}
                onRowsPerPageChange={handleSChangeRowsPerPage}
              />
            </Card>
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Card>
              <UserListToolbar
                title="Similar Colleges"
                filterName={filterName}
                onFilterName={handleFilterByName}
              />

              <Scrollbar>
                <TableContainer sx={{ minWidth: 800 }}>
                  <Table>
                    <UserListHead
                      order={order}
                      orderBy={orderBy}
                      headLabel={TABLE_HEAD}
                      rowCount={collegeList.length}
                      onRequestSort={handleRequestSort}
                    />
                    <TableBody>
                      {filteredColleges
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((row) => {
                          const { _id, name, year_founded: yearFounded, city, state } = row;

                          return (
                            <TableRow hover key={_id} tabIndex={-1}>
                              <TableCell component="th" scope="row">
                                <Stack direction="row" alignItems="center" spacing={2}>
                                  <Typography variant="subtitle2" noWrap>
                                    <RouterLink to={`/college/${_id}`}>{name}</RouterLink>
                                  </Typography>
                                </Stack>
                              </TableCell>
                              <TableCell align="left">{yearFounded}</TableCell>
                              <TableCell align="left">{city}</TableCell>
                              <TableCell align="left">{state}</TableCell>
                            </TableRow>
                          );
                        })}
                      {emptyRows > 0 && (
                        <TableRow style={{ height: 53 * emptyRows }}>
                          <TableCell colSpan={6} />
                        </TableRow>
                      )}
                    </TableBody>
                    {isCollegeNotFound && (
                      <TableBody>
                        <TableRow>
                          <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                            <SearchNotFound searchQuery={filterName} />
                          </TableCell>
                        </TableRow>
                      </TableBody>
                    )}
                  </Table>
                </TableContainer>
              </Scrollbar>

              <TablePagination
                rowsPerPageOptions={[5, 10, 25]}
                component="div"
                count={collegeList.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
}
