var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CollegeSchema = new Schema(
  {
    name: { type: String, required: true },
    year_founded: { type: String, required: true },
    city: { type: String, required: true },
    state: { type: String, required: true },
    country: { type: String, required: true },
    courses: [{ type: Schema.Types.ObjectId, ref: "Course" }],
  },
  { timestamps: false }
);

module.exports = mongoose.model("College", CollegeSchema);
