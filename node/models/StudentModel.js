var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var StudentSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    year_of_batch: { type: String, required: true },
    college_id: { type: Schema.Types.ObjectId, ref: "College", required: true },
    skills: [{ type: Schema.Types.ObjectId, ref: "Skill"}],
  },
  { timestamps: false }
);

module.exports = mongoose.model("Student", StudentSchema);
