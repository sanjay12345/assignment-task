var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CourseSchema = new Schema(
  {
    name: { type: String, required: true },
  },
  { timestamps: false }
);

module.exports = mongoose.model("Course", CourseSchema);
