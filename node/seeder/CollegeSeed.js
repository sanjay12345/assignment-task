const College = require("../models/CollegeModel");
const Student = require("../models/StudentModel");
const Course = require("../models/CourseModel");

const year_founded = [
  2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
];

const CityState = [
    {"city":"Adilabad", "state":"Andhra Pradesh"},
	{"city":"Adoni", "state":"Andhra Pradesh"},
	{"city":"Amadalavalasa", "state":"Andhra Pradesh"},
	{"city":"Amalapuram", "state":"Andhra Pradesh"},
	{"city":"Anakapalle", "state":"Andhra Pradesh"},
	{"city":"Anantapur", "state":"Andhra Pradesh"},
	{"city":"Badepalle", "state":"Andhra Pradesh"},
	{"city":"Banganapalle", "state":"Andhra Pradesh"},
	{"city":"Bapatla", "state":"Andhra Pradesh"},
	{"city":"Bellampalle", "state":"Andhra Pradesh"},
	{"city":"Bethamcherla", "state":"Andhra Pradesh"},
	{"city":"Bhadrachalam", "state":"Andhra Pradesh"},
    {"city":"Nawada", "state":"Bihar"},
	{"city":"Nokha", "state":"Bihar"},
	{"city":"Patna", "state":"Bihar"},
	{"city":"Piro", "state":"Bihar"},
	{"city":"Purnia", "state":"Bihar"},
	{"city":"Rafiganj", "state":"Bihar"},
	{"city":"Rajgir", "state":"Bihar"},
	{"city":"Ramnagar", "state":"Bihar"},
	{"city":"Raxaul Bazar", "state":"Bihar"},
	{"city":"Revelganj", "state":"Bihar"},
	{"city":"Rosera", "state":"Bihar"},
	{"city":"Saharsa", "state":"Bihar"},
    {"city":"Baloda Bazar", "state":"Chhattisgarh"},
	{"city":"Bemetra", "state":"Chhattisgarh"},
	{"city":"Bhatapara", "state":"Chhattisgarh"},
	{"city":"Bilaspur", "state":"Chhattisgarh"},
	{"city":"Birgaon", "state":"Chhattisgarh"},
	{"city":"Champa", "state":"Chhattisgarh"},
	{"city":"Chirmiri", "state":"Chhattisgarh"},
	{"city":"Dalli-Rajhara", "state":"Chhattisgarh"},
	{"city":"Dhamtari", "state":"Chhattisgarh"},
	{"city":"Dipka", "state":"Chhattisgarh"},
	{"city":"Dongargarh", "state":"Chhattisgarh"},
	{"city":"Durg-Bhilai Nagar", "state":"Chhattisgarh"},
    {"city":"Khambhat", "state":"Gujarat"},
	{"city":"Kheda", "state":"Gujarat"},
	{"city":"Khedbrahma", "state":"Gujarat"},
	{"city":"Kheralu", "state":"Gujarat"},
	{"city":"Kodinar", "state":"Gujarat"},
	{"city":"Lathi", "state":"Gujarat"},
	{"city":"Limbdi", "state":"Gujarat"},
	{"city":"Lunawada", "state":"Gujarat"},
	{"city":"Mahesana", "state":"Gujarat"},
	{"city":"Mahuva", "state":"Gujarat"},
    {"city":"Gharaunda", "state":"Haryana"},
	{"city":"Gohana", "state":"Haryana"},
	{"city":"Gurgaon", "state":"Haryana"},
	{"city":"Haibat(Yamuna Nagar)", "state":"Haryana"},
	{"city":"Hansi", "state":"Haryana"},
	{"city":"Hisar", "state":"Haryana"},
	{"city":"Hodal", "state":"Haryana"},
	{"city":"Jhajjar", "state":"Haryana"},
	{"city":"Jind", "state":"Haryana"},
	{"city":"Kaithal", "state":"Haryana"},
	{"city":"Kalan Wali", "state":"Haryana"},
	{"city":"Kalka", "state":"Haryana"},
    {"city":"Bilaspur", "state":"Himachal Pradesh"},
	{"city":"Chamba", "state":"Himachal Pradesh"},
	{"city":"Dalhousie", "state":"Himachal Pradesh"},
	{"city":"Dharamsala", "state":"Himachal Pradesh"},
	{"city":"Hamirpur", "state":"Himachal Pradesh"},
	{"city":"Mandi", "state":"Himachal Pradesh"},
	{"city":"Nahan", "state":"Himachal Pradesh"},
	{"city":"Shimla", "state":"Himachal Pradesh"},
	{"city":"Solan", "state":"Himachal Pradesh"},
];

const StudentSeed = require("./StudentSeed");

exports.CollegeSeed = async () => {
  await College.deleteMany({});
  await Student.deleteMany({});
  const coursesAll = await Course.find({});
  for (let i = 0; i < 100; i++) {
    const randomCS = Math.floor(Math.random() * CityState.length);
    const randomY = Math.floor(Math.random() * year_founded.length);
    const random = Math.floor(Math.random() * 5);

    const courses = [];
    for (let c = 0; c < random; c++) {
        const randomC = Math.floor(Math.random() * coursesAll.length);
        courses.push(coursesAll[randomC]._id);
    }
    
    const college = await College.create({
      name: `College ${i+1}`,
      year_founded: year_founded[randomY],
      city: CityState[randomCS].city,
      state: CityState[randomCS].state,
      country: "IN",
      courses: [...new Set(courses)]
    });
    console.log(`College ${i+1}`, college._id);
    await StudentSeed.StudentSeed(college._id);
  }
  console.log("College seeded");
};
