const skillData = [
  {
    name: "C++",
  },
  {
    name: "Java",
  },
  {
    name: "JavaScript",
  },
  {
    name: "Python",
  },
  {
    name: "C#",
  },
  {
    name: "PHP",
  },
  {
    name: "Ruby",
  },
  {
    name: "Swift",
  },
  {
    name: "Go",
  },
  {
    name: "Kotlin",
  },
  {
    name: "Objective-C",
  },
  {
    name: "SQL",
  },
  {
    name: "HTML",
  },
];

const Skill = require("../models/SkillModel");

exports.SkillSeed = async () => {
  await Skill.deleteMany({});
  await Skill.insertMany(skillData);
  console.log("Skill seeded");
};
