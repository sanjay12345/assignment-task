const courseData = [
  {
    name: "Computer science",
  },
  {
    name: "Electronics",
  },
  {
    name: "Mechanical",
  },
  {
    name: "Civil",
  },
  {
    name: "Electrical",
  },
  {
    name: "Chemical",
  },
  {
    name: "Biotechnology",
  },
  {
    name: "Mathematics",
  },
  {
    name: "Physics",
  },
  {
    name: "Chemistry",
  },
];

const Course = require("../models/CourseModel");

exports.CourseSeed = async () => {
  await Course.deleteMany({});
  await Course.insertMany(courseData);
  console.log("Course seeded");
};
