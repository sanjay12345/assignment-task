require("dotenv").config();
var MONGODB_URL = process.env.MONGODB_URL;
var mongoose = require("mongoose");
mongoose
  .connect(MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log("Connected to %s", MONGODB_URL);
  })
  .catch((err) => {
    console.error("App starting error:", err.message);
    process.exit(1);
  });
const CourseSeed = require("./CourseSeed");
const SkillSeed = require("./SkillSeed");
const CollegeSeed = require("./CollegeSeed");

const seedDB = async () => {
  await CourseSeed.CourseSeed();
  await SkillSeed.SkillSeed();
  await CollegeSeed.CollegeSeed();
  console.log("Database seeded");
};

seedDB().then(() => {
  mongoose.disconnect();
  process.exit(0);
});
