const Student = require("../models/StudentModel");
const Skill = require("../models/SkillModel");

const year_of_batch = [
  2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020,
];

exports.StudentSeed = async (college_id) => {
  const skillAll = await Skill.find({});
  const students = [];
  for (let i = 0; i < 100; i++) {
    const randomY = Math.floor(Math.random() * year_of_batch.length);
    const random = Math.floor(Math.random() * 5);

    const skills = [];
    for (let c = 0; c < random; c++) {
      const randomS = Math.floor(Math.random() * skillAll.length);
      skills.push(skillAll[randomS]._id);
    }
    students.push({
      name: `Student ${i+1}`,
      year_of_batch: year_of_batch[randomY],
      college_id: college_id,
      skills: [...new Set(skills)],
    });
  }
  await Student.insertMany(students);
  console.log("Student seeded");
};
