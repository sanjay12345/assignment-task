var express = require("express");
var collegeRouter = require("./college");

var app = express();

app.use("/college/", collegeRouter);

module.exports = app;