var express = require("express");
const CollegeController = require("../controllers/CollegeController");

var router = express.Router();

router.get("/", CollegeController.collegeList);
router.get("/state", CollegeController.collegeState);
router.get("/course", CollegeController.collegeCourse);
router.get("/s/:id", CollegeController.collegeSimilar);
router.get("/student/:id", CollegeController.studentDetail);
router.get("/:id", CollegeController.collegeDetail);

module.exports = router;