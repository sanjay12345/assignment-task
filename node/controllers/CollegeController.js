const College = require("../models/CollegeModel");
const Course = require("../models/CourseModel");
const Student = require("../models/StudentModel");
const Skill = require("../models/SkillModel");
const apiResponse = require("../helpers/apiResponse");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * College List.
 *
 * @returns {Object}
 */
exports.collegeList = [
  async function (req, res) {
    try {
      let filter = {};
      if (req.query.type) {
        if (req.query.type == "state") {
          filter = {
            state: req.query.value,
          };
        }
        if (req.query.type == "course") {
          const course = await Course.findOne({ name: req.query.value });
          if (course) {
            filter = {
              courses: course._id,
            };
          }
        }
      }
      College.find(filter)
        .populate("courses")
        .then((colleges) => {
          if (colleges.length > 0) {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              colleges
            );
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              []
            );
          }
        });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * College Detail.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.collegeDetail = [
  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }
    try {
      College.findOne({ _id: req.params.id })
        .populate("courses")
        .then(async (college) => {
          if (college) {
            const students = await Student.find({
              college_id: req.params.id,
            }).populate("skills");
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              { college, students }
            );
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              {}
            );
          }
        });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.collegeState = [
  function (req, res) {
    try {
      College.aggregate()
        .group({ _id: "$state", count: { $sum: 1 } })
        .exec()
        .then((colleges) => {
          if (colleges.length > 0) {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              colleges
            );
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              []
            );
          }
        });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.collegeCourse = [
  function (req, res) {
    try {
      Course.find({}).then(async (courses) => {
        if (courses.length > 0) {
          const result = [];
          for (let i = 0; i < courses.length; i++) {
            const colleges = await College.find({ courses: courses[i]._id });
            result.push({
              _id: courses[i].name,
              count: colleges.length,
            });
          }
          return apiResponse.successResponseWithData(
            res,
            "Operation success",
            result
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "Operation success",
            []
          );
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.collegeSimilar = [
  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }
    try {
      College.findOne({ _id: req.params.id })
        .populate("courses")
        .then(async (college) => {
          if (college) {
            console.log(college.courses.map((c) => c._id));
            College.find({
              courses: { $in: college.courses.map((c) => c._id) },
            }).then((colleges) => {
              if (colleges.length > 0) {
                return apiResponse.successResponseWithData(
                  res,
                  "Operation success",
                  colleges
                );
              } else {
                return apiResponse.successResponseWithData(
                  res,
                  "Operation success",
                  []
                );
              }
            });
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              {}
            );
          }
        });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


exports.studentDetail = [
  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }
    try {
      Student.findOne({ _id: req.params.id })
        .populate("skills")
        .then(async (student) => {
          if (student) {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              student
            );
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              {}
            );
          }
        });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
